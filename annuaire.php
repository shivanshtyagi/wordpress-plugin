<?php
/*
Plugin Name: Plugin Annuaire Connection
Plugin URI: http://portfolio-shivansh.surge.sh/
Description: Create a plugin for contact directory
Author: Shivansh
Version: 1.0
Author URI: http://portfolio-shivansh.surge.sh/
*/

function shortcode_connection() {
  global $wpdb;
  $results = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}annuaire", OBJECT );

  foreach($results as $entreprise){
    echo "<div>
    
  
    <h2>{$entreprise -> id }) Entreprise : {$entreprise -> nom_entreprise}</h2>
    <h3>Localisation : {$entreprise -> localisation_entreprise}</h3>
    <h3>Contact : {$entreprise -> prenom_contact} 
                  {$entreprise -> nom_contact}</br> 
                  {$entreprise -> mail_contact}</h3>
    
    </div>";
     
  }
}

add_shortcode('connection', 'shortcode_connection');

